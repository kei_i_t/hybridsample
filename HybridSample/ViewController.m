//
//  ViewController.m
//  HybridSample
//
//  Created by ikata on 2015/09/14.
//  Copyright (c) 2015年 ikata. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()<UITabBarDelegate, UIWebViewDelegate>{
    
    __weak IBOutlet UITabBar *_tabbar;
    __weak IBOutlet UITabBarItem *_itemHome;
    __weak IBOutlet UITabBarItem *_itemProducts;
    __weak IBOutlet UITabBarItem *_itemCompany;
    __weak IBOutlet UITabBarItem *_itemContacts;
    __weak IBOutlet UITabBarItem *_itemAbout;
    
    __weak IBOutlet UIWebView *_webview;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    _tabbar.delegate = self;
    _webview.delegate = self;
    
    [self setTitle:@"ぷらすわんアプリ"];
    
    [_tabbar setSelectedItem:_itemHome];
    [_webview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://plusone-appli.com"]]];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item{
    if([item isEqual:_itemHome]){
        [_webview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://plusone-appli.com"]]];
    }else if([item isEqual:_itemCompany]){
        [_webview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://plusone-appli.com/about/"]]];
    }else if([item isEqual:_itemContacts]){
        [_webview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://plusone-appli.com/contact_us/"]]];
    }else if([item isEqual:_itemProducts]){
        [_webview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://plusone-appli.com/works/"]]];
    }else if([item isEqual:_itemAbout]){
        [_webview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://plusone-appli.com/services/"]]];
    }
}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    if(![request.URL.absoluteString hasPrefix:@"http://plusone-appli.com"]){
        
        if([request.URL.absoluteString hasPrefix:@"http://static.ak.facebook.com/connect/"] ||
           [request.URL.absoluteString hasPrefix:@"https://s-static.ak.facebook.com/connect/xd_arbiter/"] ||
           [request.URL.absoluteString hasPrefix:@"http://www.facebook.com/plugins/"] ||
           [request.URL.absoluteString hasPrefix:@"https://www.facebook.com/plugins/"] ||
           [request.URL.absoluteString hasPrefix:@"https://m.facebook.com/plugins/"] ||
           [request.URL.absoluteString hasPrefix:@"https://www.facebook.com/connect/"] ||
           [request.URL.absoluteString hasPrefix:@"about:blank"] ||
           [request.URL.absoluteString hasPrefix:@"https://widgets.getpocket.com/v1/button"]){
            return YES;
        }
        
        NSLog(@"%@", request.URL.absoluteString);
        UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"警告" message:@"外部サイトへのアクセスは禁止しています。" preferredStyle:UIAlertControllerStyleAlert];
        
        [controller addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil]];
        [self presentViewController:controller animated:YES completion:nil];
        return NO;
    }
    return YES;
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    self.title = [webView stringByEvaluatingJavaScriptFromString:@"document.title"];
}

@end
