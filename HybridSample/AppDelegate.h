//
//  AppDelegate.h
//  HybridSample
//
//  Created by ikata on 2015/09/14.
//  Copyright (c) 2015年 ikata. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

